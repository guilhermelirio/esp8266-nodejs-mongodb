## ESP8266-NODEJS-MONGODB

Exemplo de uma aplicação onde clientes (ESP8266) se conectam à um servidor em NodeJS com banco de dados em MongoDB

---

## Fluxograma

![Fluxograma](https://bytebucket.org/guilhermelirio/esp8266-nodejs-mongodb/raw/042aff8e3752d9130b4c555cb149f2b2147b3d89/fluxograma.png)


---

## Instalação

```sh
$ git clone https://bitbucket.org/guilhermelirio/esp8266-nodejs-mongodb.git
$ cd esp8266-nodejs-mongodb
$ npm install
```

## Como Usar

1. Renomeie o arquivo ```.env.example``` para ```.env```
2. Abra o arquivo .env e edite as informações (não as chaves). Você pode criar qualquer ```TOKEN_SECRET``` e ```AUTH_SERVER_KEY```. A chave ```DB_URL``` é o endereço do seu banco de dados MongoDB.
3.```$ npm start```
4. Faça o upload do código para o esp8266, alterando o **WIFI_SSID**, **WIFI_PASSWORD**, **API_URL** e **auth-server-key**.

