const express = require("express")
const devicesModel = require("../models/devices")
const dataModel = require("../models/addData")
const jwt = require('jsonwebtoken')
const rota = express.Router()
const bcrypt = require("bcryptjs")
const auth = require("../functions/auth")

//Rota de autenticação
rota.post("/authorization", async (req, res) =>{

    //Captura o auth-server-key
    var serverKey = req.headers['auth-server-key'];

    //Caso não tenha, exibe um erro.
    if (!serverKey) return res.status(401).send({ status: "ERRO", auth: false, msg: 'Nenhum auth-server-key fornecido na requisicao.' });

    //Caso tenha, mas seja diferente, exibe um erro.
    if(serverKey != process.env.AUTH_SERVER_KEY) return res.status(401).send({ status: "ERRO", auth: false, msg: 'Falha ao autenticar o auth-server-key!' });

    //É feita uma desestruturação (ES6) do conteúdo vindo do ESP8266
    const { deviceID, password } = req.body

    try{
        //Irá procurar no BD um deviceID igual ao enviado
        const dadosDeviceID = await devicesModel.findOne({deviceID}).select('+password')

        //Se não encontrar o dispositivo com o mesmo deviceID, será criado e autenticado.
        if(!dadosDeviceID){

            console.log('Dispositivo não existe, será criado e autenticado!')

            //Cadastra o disposivito no BD
            const criaDevice = await devicesModel.create(req.body)

            //Cria um token de acesso incluindo o ID do device, com 24 hrs de expiração
            const token = jwt.sign({ id: deviceID }, process.env.TOKEN_SECRET, {
                expiresIn: 86400
            })

            //Retorna o token, que será usado no ESP8266 (TOKEN)
            return res.status(200).send({ status: "OK", msg: "Dispositivo criado e autenticado com sucesso!", token: token})
        
        }
        
        //Vai comparar a senha enviada (password), com a senha cadastrada no banco (devicesModel.password)
        if(!await bcrypt.compare(password, dadosDeviceID.password)){
            
            console.log('Verificando a senha...')
            return res.status(400).send({status: "ERRO", msg: "Senhas não conferem." }) 
           
        }

        console.log('Dispositivo já existe, será apenas autenticado!')

        //Define a senha como undefined
        dadosDeviceID.password = undefined

        //Cria um token de acesso incluindo o ID do usuário, com 24 hrs de expiração
        const token = jwt.sign({ id: deviceID }, process.env.TOKEN_SECRET, {
            expiresIn: 86400
        })

        return res.status(200).send({status: "OK", msg: "Usuario autenticado com sucesso!", token: token})        

    } catch(err){
        console.log(err)
        return res.status(400).send({status: "ERRO", erro: err.message})
    }          

})

//Rota para adicionar os dados vindos do ESP8266
rota.post('/addData', auth.verifyJWT, async (req, res) => {
    //Vindo da função VerifyToken
    const deviceID =  req.deviceID
    try{
        //Adiciona no BD
        const addDados = await dataModel.create({deviceID: deviceID, ...req.body})
        return res.status(200).send({status: "OK", msg: "Dados adicionados com sucesso!"})
    }catch(err){
        console.log(err)
        return res.status(400).send({status: "ERRO", msg: "Erro ao adicionar dados"})
    }
   
})


module.exports = (app) => app.use("/api", rota)