const mongoose = require("mongoose")
const log = require('console-emoji')

const uri = process.env.DB_URL
mongoose.Promise = global.Promise

mongoose.connect(uri, { useNewUrlParser: true , useCreateIndex: true});

var db = mongoose.connection;

var twirlTimer = (function() {
	var P = [
		"⬆️",
		"↗️",
		"➡️",
		"↘️",
		"⬇️",
		"↙️",
		"⬅️",
		"↖️"
	]
	var x = 0;
	return setInterval(function() {
	  process.stdout.write("\r" + P[x++]+'  Conectando ao BD...');
	  x &= 7;
	}, 250);
  })();

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  setTimeout(() =>{
		clearInterval(twirlTimer)
		process.stdout.write("\n")
		log("Conectado com sucesso ao BD!", 'ok')
	},1000)
});

module.exports = mongoose