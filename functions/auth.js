const jwt = require('jsonwebtoken');

function verifyJWT(req, res, next){
    var token = req.headers['x-access-token'];

    if (!token) return res.status(401).send({ status: "ERRO", auth: false, msg: 'Nenhum token fornecido na requisição.' });

    jwt.verify(token, process.env.TOKEN_SECRET, function(err, decoded) {
      if (err){
        if(err.name == "TokenExpiredError") return res.status(500).send({status: "ERRO", auth: false, msg: "Expired Token"});

        return res.status(500).send({ status: "ERRO", auth: false, msg: 'Falha ao autenticar o token!' });
      }     
      
      //console.log(decoded)
      //Se tudo estiver correto, salva no request para uso posterior      
      req.deviceID = decoded.id;
      next();
    });
  }

  module.exports.verifyJWT = verifyJWT;