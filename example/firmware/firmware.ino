 /*
  * Exemplo de conexão com um servidor em NodeJS, usando autenticação JWT  * 
  * 
  * Autor: Guilherme Lirio Tomasi de Oliveira *
  * 
  * Agradecimento: 
  * - Douglas Salim Zuqueto, por ter disponibilizado o código .ino o qual
  * me inspirei.
  * 
  */

/************************** Inclusão das Bibliotecas **************************/

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <MD5.h>

/**************************** DEBUG *******************************/

#define DEBUG_PRINTLN(m) Serial.println(m)
#define DEBUG_PRINT(m) Serial.print(m)

/**************************** Variaveis globais *******************************/

#define WIFI_SSID     "SEU_SSID"
#define WIFI_PASSWORD "SENHA_WIFI"

String API_URL = "http://IP_DO_SERVIDOR/api";
String TOKEN;
char JSONR[300] = "";
char JSONSD[100] = "";
uint32_t sleep_time = 60 * 2000000; //intervalo de 2 minutos

/************************* Declaração dos Prototypes **************************/

void initSerial();
void initWiFi();
void requestAccess();
bool sendData();
String createMD5(String chipID);
void handleError(int httpCode , String message);

/************************* Instanciação dos objetos  **************************/

WiFiClient client;
HTTPClient http;
MD5Builder md5;

void initSerial() {
  Serial.begin(115200);
}

//initWiFi()

void handleError(int httpCode , String message ) {
  DEBUG_PRINT("[ERRO]: Code: " + String(httpCode));
  DEBUG_PRINTLN(" | Message: " + message);
}

void requestAccess() {
  String dados;

  dados += "{";
  dados += "\"deviceID\":";
  dados += "\""+createMD5()+"\"";
  dados += ",\"password\":";
  dados += "\"41752017\"";
  dados += "}";

  http.begin(API_URL + "/authorization");

  http.addHeader("Content-type", "application/json", false, true);
  http.addHeader("auth-server-key", "COLOQUE_AQUI_SUA_AUTH_SERVER_KEY");

  int httpCode = http.POST(dados);

  String response =  http.getString();

  //DEBUG_PRINTLN("[REQUEST ACCESS => JSON RESPONSE]: "+response);

  //JSON PARSING
  const size_t BUFFER_JSON = JSON_OBJECT_SIZE(3) + 281;
  DynamicJsonBuffer jsonBuffer(BUFFER_JSON); 

  //Tranforma a String response em Char Array
  response.toCharArray(JSONR, 300);
   
  JsonObject& arrayJson = jsonBuffer.parseObject(JSONR);  

  if (!arrayJson.success()) {
    DEBUG_PRINTLN("[ERRO]: parseObject() falhou. => A resposta do servidor nao e um JSON. Verifique a conexao.");
    return;
  } 

  //Recupera os valores do response em JSON.
  const char* statusResponse = arrayJson["status"];
  const char* tokenResponse = arrayJson["token"];
  const char* msgResponse = arrayJson["msg"];
  
  http.end();

  DEBUG_PRINTLN((String)"[HTTP][Request Access]: "+msgResponse);

  if (httpCode != HTTP_CODE_OK) {
    handleError(httpCode, String(msgResponse));
    return;
  }

  DEBUG_PRINTLN((String)"[Token]: "+tokenResponse);
  
  //Seta a variável global TOKEN, com o token vindo do response JSON
  TOKEN = tokenResponse;
}

bool sendData() {

  String p;
  p += "{";
  p += "\"dado01\":";
  p += "\"25.6\"";
  p += ",\"dado02\":";
  p += "\"95\"";
  p += "}";

  String uri = API_URL + "/addData";

  http.begin(uri);

  http.addHeader("Content-type", "application/json", false, true);
  http.addHeader("x-access-token", TOKEN);
  
  int httpCode = http.POST(p);  

  String response =  http.getString();

  //JSON PARSING
  const size_t BUFFER_JSON2 = JSON_OBJECT_SIZE(3) + 50;
  DynamicJsonBuffer jsonBuffer(BUFFER_JSON2); 

  //Tranforma a String response em Char Array
  response.toCharArray(JSONSD, 100);
   
  JsonObject& arrayJson = jsonBuffer.parseObject(JSONSD);  

  if (!arrayJson.success()) {
    DEBUG_PRINTLN("[ERRO]: parseObject() falhou. => A resposta do servidor nao e um JSON. Verifique a conexao.");
    return false;
  } 

  //Recupera os valores do response em JSON.
  const char* statusResponseSD = arrayJson["status"];
  const char* tokenResponseSD = arrayJson["token"];
  const char* msgResponseSD = arrayJson["msg"];

  if((String)tokenResponseSD == "Expired Token"){
    handleError(httpCode, response);
    requestAccess();
    sendData();
    return true;
  }

  http.end();

  if (httpCode != HTTP_CODE_OK) {
    handleError(httpCode, String(msgResponseSD));
    return false;
  }   

  DEBUG_PRINTLN((String)"[HTTP][sendData]: " + msgResponseSD);
  return true;
}

String createMD5(){
  String chipID = String(ESP.getChipId());
  md5.begin();
  md5.add(chipID);
  md5.calculate();
  return md5.toString();
}

/********************************** Sketch ************************************/

void setup() { 

  initSerial();
  initWiFi();  
  requestAccess();
  sendData();   
  DEBUG_PRINTLN("[ESP8266]: Sleeping...");
  ESP.deepSleep(sleep_time);   
}

void loop() {
}

