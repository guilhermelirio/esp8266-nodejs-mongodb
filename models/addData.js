const mongoose = require("../database")
const bcrypt = require("bcryptjs")

const addDataSchema = new mongoose.Schema({
    deviceID: {
        type: String,
        required: true
    },
    dado01: {
        type: String,
        required: true,
    },
    dado02: {
        type: String,
        required: true,
    },
    criadoEm:{
        type: Date,
        default: Date.now,
    }
});

const addData = mongoose.model("dado", addDataSchema);

module.exports = addData;