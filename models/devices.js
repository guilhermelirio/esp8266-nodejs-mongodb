const mongoose = require("../database")
const bcrypt = require("bcryptjs")

const DeviceSchema = new mongoose.Schema({
    deviceID: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        select: false, //para a senha não entrar no array de busca
    },
    criadoEm:{
        type: Date,
        default: Date.now,
    }
});

DeviceSchema.pre('save', async function(next){
    //Faz a criptação da senha
    const hash = await bcrypt.hash(this.password, 10)
    this.password = hash

    next();
})

const Device = mongoose.model("Device", DeviceSchema);

module.exports = Device;